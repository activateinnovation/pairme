//
//  AVCamPreviewView.h
//  PairMe
//
//  Created by Taylor Korensky on 4/6/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//


#import <UIKit/UIKit.h>

@class AVCaptureSession;

@interface AVCamPreviewView : UIView

@property (nonatomic) AVCaptureSession *session;

@end