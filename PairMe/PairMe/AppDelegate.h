//
//  AppDelegate.h
//  PairMe
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

