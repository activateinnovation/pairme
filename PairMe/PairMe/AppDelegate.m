//
//  AppDelegate.m
//  PairMe
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //[Parse enableLocalDatastore];
    
    // Initialize Parse.
    [Parse setApplicationId:@"eqtlFbFkxpOWZi2GuyUJMP1Gyi5vZgRYklrCAcQa"
                  clientKey:@"mZZPJJiFvDGRBoe6a4HNsKMT5wX1F41LvzVhrL3G"];
    
    UIUserNotificationType userNotificationTypes = (
                                                    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge
                        
                                                    );
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    // [Optional] Track statistics around application opens.
    // [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
        return YES;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current Installation and save it to Parse.
    NSLog(@"Device Token: %@", deviceToken);
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //[PFPush handlePush:userInfo];
    
    NSLog(@"User Info Push: %@", userInfo);
    NSString *deviceName = [[UIDevice currentDevice] name];
    if([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] isEqualToString:@"Check Status"]){
        
        NSDictionary *data;
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"status"]){
            
            data = @{
                     @"alert" : @"Checked Status",
                     @"name" : deviceName,
                     @"status" : @"Enabled"
                     };
        }
        else{
            
            data = @{
                     @"alert" : @"Checked Status",
                     @"name" : deviceName,
                     @"status" : @"Disabled"
                    };
            
        }
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"objectId" equalTo: [userInfo objectForKey: @"name"]];
        
        // Send push notification to query
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];


    }
    else{
    
        [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]] object:nil userInfo:userInfo];
    }
    //Take Photo Notification
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
