//
//  CaptureViewController.h
//  PairMe
//
//  Created by Taylor Korensky on 4/6/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "StartingViewController.h"
#import "UIImage+ResizeMagick.h"




@interface CaptureViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *backButton;



@end
