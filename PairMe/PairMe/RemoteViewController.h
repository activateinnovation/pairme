//
//  RemoteViewController.h
//  PairMe
//
//  Created by Taylor Korensky on 4/1/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RemoteViewController : UIViewController <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *takePicture;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *startVideoButton;
@property (strong, nonatomic) NSMutableArray *recordingDevice;
@property BOOL isRecording;
-(void) setUpView : (NSMutableArray *)device;
- (IBAction)takePictureClicked:(id)sender;
@property (strong, nonatomic) UIButton *cancelButton;
- (IBAction)startStopVideo:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *cameraDevicePicker;
- (IBAction)cameraDeviceChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *recordingCircle;
@property NSTimeInterval startTime;
@property BOOL captureDeviceStatus;
@property BOOL inCheckForPhoto;
@property BOOL inCheckForVideo;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSMutableArray *devicesArray;

@property (strong, nonatomic) UIView *successView;

@end
