//
//  RemoteViewController.m
//  PairMe
//
//  Created by Taylor Korensky on 4/1/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import "RemoteViewController.h"

@interface RemoteViewController ()

@end

@implementation RemoteViewController
@synthesize recordingDevice;
@synthesize cancelButton;

-(void) viewWillAppear:(BOOL)animated{
    
    self.takePicture.layer.shadowColor = [UIColor blackColor].CGColor;
    self.takePicture.layer.shadowOpacity = 0.5;
    self.takePicture.layer.shadowRadius = 2.5;
    self.takePicture.layer.shadowOffset = CGSizeMake(0.0f,1.0f);
    
    self.startVideoButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.startVideoButton.layer.shadowOpacity = 0.5;
    self.startVideoButton.layer.shadowRadius = 2.5;
    self.startVideoButton.layer.shadowOffset = CGSizeMake(0.0f,1.0f);

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"status"];
    
    [super viewWillAppear:animated];
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"background.png"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotStatus:) name:@"Checked Status" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setImagePreview:) name:@"Preview" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captured:) name:@"Captured" object: nil];
    self.inCheckForPhoto = NO;
    self.inCheckForVideo = NO;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
}

-(void) viewWillDisappear:(BOOL)animated{
    
    self.recordingDevice = nil;
}

-(void) viewDidAppear:(BOOL)animated{
    
    [self performSelectorOnMainThread:@selector(pushToCheckStatus) withObject:nil waitUntilDone:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setUpView : (NSMutableArray *)device {
    
    self.devicesArray = [[NSMutableArray alloc] init];
    self.devicesArray = device;
   // self.recordingDevice = [[NSMutableArray alloc] init];
    //self.recordingDevice = device;
    NSString *string = [[NSString alloc] init];
    string = [NSString stringWithFormat:@"Remote for %@", device[0][1]];
    for(int x = 1; x < self.devicesArray.count; x++){
        
        string = [NSString stringWithFormat:@"%@ - %@", string, device[x][1]];
    }
    self.titleLabel.text = string;
    self.isRecording = NO;
    [self.startVideoButton setTitle:@"Start Video" forState:UIControlStateNormal];
    
    
    [self.timeLabel setHidden:YES];
    [self.recordingCircle setHidden:YES];
    
    [self.cameraDevicePicker setSelectedSegmentIndex:1];
    
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 65, 30)];
    [cancelButton setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Back" forState:UIControlStateNormal];
    [cancelButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont fontWithName:@"Noteworthy-bold" size:18]];
    [cancelButton.layer setCornerRadius:8.0f];
    [cancelButton setClipsToBounds:YES];
    [cancelButton setAlpha:0.8f];
    [self.view addSubview:cancelButton];
    
    self.successView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height - 100, 200, 65)];
    [self.successView setBackgroundColor:[UIColor lightGrayColor]];
    [self.successView setAlpha:0.85];
    [self.successView.layer setCornerRadius:8.0f];
    [self.successView setHidden:YES];
    [self.view addSubview:self.successView];
  
    self.successView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.successView.layer.shadowOpacity = 0.5;
    self.successView.layer.shadowRadius = 2.5;
    self.successView.layer.shadowOffset = CGSizeMake(1.0f,1.0f);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.successView.frame.size.height/2 - 15, self.successView.frame.size.width, 30)];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:@"Capture Success"];
    [label setFont:[UIFont fontWithName:@"Noteworthy-bold" size:18]];
    [label setTextColor:[UIColor whiteColor]];
    [self.successView addSubview:label];

}

-(void) pushToCheckStatus {
    
    for(int x = 0; x < self.devicesArray.count; x++){
        NSDictionary *data = @{
                               @"alert" : @"Check Status",
                               @"name" : [PFInstallation currentInstallation].objectId,
                               @"status" : @"new"
                               };
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]];
        // Set channe
        [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
        // Send push notification to query
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
    }
}

-(void) gotStatus: (NSNotification *) notification {
    
    
    NSLog(@"Checked Status: %@", [[notification userInfo] objectForKey:@"status"]);
    
    if([[[notification userInfo] objectForKey:@"status"] isEqualToString:@"Disabled"]){
        
        self.captureDeviceStatus = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Capture Disabled" message:[NSString stringWithFormat:@"%@ is not in capture mode. Make sure the device has PairMe open and logged in. Would you like to turn on capture mode?", [[notification userInfo] objectForKey:@"name"]] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [alert show];
    }
    else{
        
        self.captureDeviceStatus = YES;
        
        if(self.inCheckForPhoto == YES){
            
            [self takePhotoAfterDelay];
            self.inCheckForPhoto = NO;
        }
        
        if(self.inCheckForVideo == YES){
            
            [self startStopVideoAfterDelay];
            self.inCheckForVideo = NO;
        }
    }
    
    
}

-(void) setImagePreview: (NSNotification *)notification {
    
    NSString *image = [[notification userInfo] objectForKey:@"image"];
    NSLog(@"Image: %@", image);
    NSData *data = [[NSData alloc] initWithBase64EncodedString:image options:0];
    [self.imageView setImage:[UIImage imageWithData:data]];

}

- (IBAction)takePictureClicked:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.inCheckForPhoto = YES;
        [self pushToCheckStatus];
    });
}

-(void)takePhotoAfterDelay{
    
    if(self.captureDeviceStatus == YES){
        
        for(int x = 0; x < self.devicesArray.count; x++){
        NSDictionary *data = @{
                               @"alert" : @"Take Photo",
                               @"name" : [PFInstallation currentInstallation].objectId,
                               @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                               };
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
        [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
        
        // Send push notification to query
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
        }
        self.inCheckForPhoto = NO;
            
    }
    
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        
        NSLog(@"Enable Device Yes");
        [self enableDevice];
    }
    else{
        
    }
    
}

-(void) enableDevice {
    
    for(int x = 0; x < self.devicesArray.count; x++){
        NSDictionary *data = @{
                           @"alert" : @"Open Camera",
                           @"name" : [PFInstallation currentInstallation].objectId,
                           @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                           };
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
        [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
    
        // Send push notification to query
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
    }
}

-(void) backButtonClicked{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) updateTimer {
    
    if(self.isRecording == NO){
        
        return;
    }
    
    //NSLog(@"Update");
    
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsedTime = currentTime - self.startTime;
    
    // NSLog(@"currentTimeString: %f", elapsedTime);
    
    // int hours = (int)(elapsedTime/ 3600);
    int minutes = (int)(elapsedTime / 60.0);
    int seconds = (int)(elapsedTime = elapsedTime - (minutes * 60));
    //int milliseconds = (int)(elapsedTime * 100) % 100;
    // NSLog(@"Milliseconds: %02d", milliseconds);
    
    NSString *currentTimeString = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    
    
    [self.timeLabel setText: currentTimeString];
    
    [self performSelector:@selector(updateTimer) withObject:self afterDelay:0.05];
    
}



- (IBAction)startStopVideo:(id)sender {
    
    if(self.captureDeviceStatus == YES){
        
        if(self.isRecording == NO){
            [self.cancelButton setEnabled:NO];
            self.isRecording = YES;
            [self.timeLabel setHidden:NO];
            [self.recordingCircle setHidden:NO];
            self.startTime = [NSDate timeIntervalSinceReferenceDate] + 1;
            [self.startVideoButton setTitle:@"Stop Video" forState:UIControlStateNormal];
                     [self updateTimer];
        }
        else{
            [self.cancelButton setEnabled:YES];
            self.isRecording = NO;
            [self.startVideoButton setTitle:@"Start Video" forState:UIControlStateNormal];
            [self blinkTimeAndRemove];
        }

        for(int x = 0; x < self.devicesArray.count; x++){
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
        [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
        
        if(self.isRecording == YES){
          
            NSDictionary *data = @{
                                   @"alert" : @"Start Video",
                                   @"name" : [PFInstallation currentInstallation].objectId,
                                   @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                                   };
            
            // Send push notification to query
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
          
   
        }
        else{
            
            NSDictionary *data = @{
                                   @"alert" : @"Stop Video",
                                   @"name" : [PFInstallation currentInstallation].objectId,
                                   @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                                   };
            
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
          
        }
        }
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
                [self pushToCheckStatus];
                self.inCheckForVideo = YES;
        });
            
    
    }
        

}


-(void) startStopVideoAfterDelay {
    
    if(self.captureDeviceStatus == YES) {
        
        if(self.isRecording == NO){
            
            [self.cancelButton setEnabled:NO];
            self.isRecording = YES;
            [self.timeLabel setHidden:NO];
            [self.recordingCircle setHidden:NO];
            self.startTime = [NSDate timeIntervalSinceReferenceDate] + 1;
            [self.startVideoButton setTitle:@"Stop Video" forState:UIControlStateNormal];
            [self updateTimer];
        }
        else{
            [self.cancelButton setEnabled:YES];
            self.isRecording = NO;
            [self.startVideoButton setTitle:@"Start Video" forState:UIControlStateNormal];
            [self blinkTimeAndRemove];
        }

        
        for(int x = 0; x < self.devicesArray.count; x++){

        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
        [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
        
        if(self.isRecording == NO){

            NSDictionary *data = @{
                                   @"alert" : @"Start Video",
                                   @"name" : [PFInstallation currentInstallation].objectId,
                                   @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                                   };
            
            // Send push notification to query
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
    
        }
        else{
            
            
            NSDictionary *data = @{
                                   @"alert" : @"Stop Video",
                                   @"name" : [PFInstallation currentInstallation].objectId,
                                   @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                                   };
            
            
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
        }
        }
        
        self.inCheckForVideo = NO;
    }
    
}


-(void) blinkTimeAndRemove {
    
    [UIView animateWithDuration:1.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.timeLabel setAlpha:0.0f];
        [self.recordingCircle setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.timeLabel setAlpha:1.0f];
            [self.recordingCircle setAlpha:1.0f];
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:1.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self.timeLabel setAlpha:0.0f];
                [self.recordingCircle setAlpha:0.0f];
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [self.timeLabel setAlpha:1.0f];
                    [self.recordingCircle setAlpha:1.0f];
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:1.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                        [self.timeLabel setAlpha:0.0f];
                        [self.recordingCircle setAlpha:0.0f];
                        
                    } completion:^(BOOL finished) {
                        [self.timeLabel setHidden:YES];
                        [self.recordingCircle setHidden:YES];
                        [self.timeLabel setAlpha:1.0f];
                        [self.recordingCircle setAlpha:1.0f];
                        
                    }];
                    
                    
                }];
                
                
            }];
            
            
            
        }];
        
    }];
    
}


- (IBAction)cameraDeviceChanged:(id)sender {
    
    
    for(int x = 0; x < self.devicesArray.count; x++){

    NSDictionary *data = @{
                           @"alert" : @"Change Camera",
                           @"name" : [PFInstallation currentInstallation].objectId,
                           @"camera" : [self.cameraDevicePicker titleForSegmentAtIndex:[self.cameraDevicePicker selectedSegmentIndex]]
                           };
    
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
    [pushQuery whereKey:@"devicename" equalTo: self.devicesArray[x][1]];
    
    // Send push notification to query
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setData:data];
    [push sendPushInBackground];
    }
}

-(void) captured: (NSNotification *) notification
{
    
    [self.successView setHidden:NO];
    [UIView animateWithDuration:2.0 delay:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [self.successView setAlpha:0.0f];
        
    } completion:^(BOOL finished) {
        
        [self.successView setHidden:YES];
        [self.successView setAlpha:1.0f];
        
    }];
    
}
@end
