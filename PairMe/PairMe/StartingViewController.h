//
//  StartingViewController.h
//  PairMeENGAVFoundationCameraControllerENGAVFoundationCameraController
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "RemoteViewController.h"
#import "CaptureViewController.h"
#import "ODRefreshControl.h"
//#import <LLSimpleCamera/LLSimpleCamera.h>
#import "ViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface StartingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *devicesArray;
- (IBAction)remoteClicked:(id)sender;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UIView *blackoutView;
- (IBAction)captureClicked:(id)sender;
@property (strong, nonatomic) UIButton *cancelButton;

@property (strong, nonatomic) UIImageView *imageView;
@property BOOL cameraOpen;
- (IBAction)logOut:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *logOutButton;
@property (strong, nonatomic) IBOutlet UIButton *remoteButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *captureButtonOutlet;
@property (strong, nonatomic) NSMutableArray *selectedDevicesArray;
@end
