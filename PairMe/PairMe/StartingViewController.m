//
//  StartingViewController.m
//  PairMe
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import "StartingViewController.h"

@interface StartingViewController ()

@end

@implementation StartingViewController
@synthesize cancelButton;
@synthesize imageView;


-(void) viewWillAppear:(BOOL)animated{
    
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"status"];
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkStatus:) name:@"Check Status" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCamera:) name:@"Open Camera" object:nil];
    self.selectedDevicesArray = [[NSMutableArray alloc] init];
    
    [self.logOutButton.layer setCornerRadius:8.0f];
    [self.logOutButton setAlpha:0.8];
    [self.logOutButton setClipsToBounds:YES];
    
    self.remoteButtonOutlet.layer.shadowColor = [UIColor blackColor].CGColor;
    self.remoteButtonOutlet.layer.shadowOpacity = 0.5;
    self.remoteButtonOutlet.layer.shadowRadius = 2.5;
    self.remoteButtonOutlet.layer.shadowOffset = CGSizeMake(0.0f,1.0f);
    
    self.captureButtonOutlet.layer.shadowColor = [UIColor blackColor].CGColor;
    self.captureButtonOutlet.layer.shadowOpacity = 0.5;
    self.captureButtonOutlet.layer.shadowRadius = 2.5;
    self.captureButtonOutlet.layer.shadowOffset = CGSizeMake(0.0f,1.0f);
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"background.png"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
              self.imageView  = [[UIImageView alloc] init];
        self.devicesArray = [[NSMutableArray alloc] init];

    if([[PFUser currentUser][@"devices"] count] > 0){
        
         self.devicesArray = [[PFUser currentUser][@"devices"] mutableCopy];
    }
    
    BOOL found = NO;
    for(int x = 0; x < self.devicesArray.count; x++){
        
        if([[[self.devicesArray objectAtIndex:x] objectAtIndex:1] isEqualToString:[PFInstallation currentInstallation][@"devicename"]]){
            
            [[self.devicesArray objectAtIndex:x] setObject:[PFInstallation currentInstallation].objectId atIndex:0];
            
            
            found = YES;
            break;
        }
    }
    
    if(found == NO) {
        
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        [temp addObject: [PFInstallation currentInstallation].objectId];
        [temp addObject:[[UIDevice currentDevice] name]];
        [self.devicesArray addObject: temp];
    }
   
    NSLog(@"Devices Array: %@", self.devicesArray);
    
    [PFUser currentUser][@"devices"] = self.devicesArray;
    [[PFUser currentUser] saveInBackground];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) openCamera: (NSNotification *)notification {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CaptureViewController *remoteView = [storyboard instantiateViewControllerWithIdentifier:@"capture"];
        [self presentViewController: remoteView animated:YES completion:^{
           
            NSDictionary *data = @{
                                   @"alert" : @"Checked Status",
                                   @"name" : @"enabled",
                                   @"status" : @"Enabled"
                                   };
            
            PFQuery *pushQuery = [PFInstallation query];
            [pushQuery whereKey:@"objectId" equalTo: [[notification userInfo] objectForKey: @"name"]];
            
            // Send push notification to query
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
        }];
}

- (IBAction)remoteClicked:(id)sender {
   
    [[PFUser currentUser] fetch];
    
    self.blackoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.blackoutView setBackgroundColor:[UIColor blackColor]];
    [self.blackoutView setAlpha:0.5f];
    [self.view addSubview:self.blackoutView];
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.1, self.view.frame.size.height *0.07, self.view.frame.size.width * 0.8, self.view.frame.size.height * 0.86 )];
    [self.containerView setBackgroundColor:[UIColor whiteColor]];
    [self.containerView.layer setCornerRadius:8.0f];
    [self.view addSubview:self.containerView];
    
    
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 30, self.containerView.frame.size.width - 50, 80)];
    [descLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [descLabel setNumberOfLines:3];
    [descLabel setFont:[UIFont fontWithName:@"Noteworthy-bold" size:17]];
    [descLabel setTextColor:[UIColor darkGrayColor]];
    [descLabel setTextAlignment:NSTextAlignmentCenter];
    [descLabel setText:@"Please select the device you would like to record or capture from.\n(pull to refresh)"];
    
    [self.containerView addSubview:descLabel];
 
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.containerView.frame.size.height * 0.2, self.containerView.frame.size.width, self.containerView.frame.size.height * 0.6)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.containerView addSubview:self.tableView];

    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [refreshControl setTintColor:[UIColor colorWithRed:(60/255.f) green:(142/255.f) blue:(162/255.f) alpha:1.0f]];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];

 
    UIButton *cancelButton1 = [[UIButton alloc] initWithFrame:CGRectMake(self.containerView.frame.size.width *0.05, self.containerView.frame.size.height - 80 , self.containerView.frame.size.width *0.40, 50)];
    [cancelButton1 setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    [cancelButton1 addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton1.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [cancelButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton1.titleLabel setFont:[UIFont fontWithName:@"Noteworthy-bold" size:20]];
    [cancelButton1.layer setCornerRadius:8.0f];
    [cancelButton1 setAlpha:0.8];
    [cancelButton1 setClipsToBounds:YES];
    [self.containerView addSubview:cancelButton1];
    
    UIButton *goButton = [[UIButton alloc] initWithFrame:CGRectMake(self.containerView.frame.size.width *0.55, self.containerView.frame.size.height - 80 , self.containerView.frame.size.width *0.40, 50)];
    [goButton setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    [goButton addTarget:self action:@selector(goToRemoteView) forControlEvents:UIControlEventTouchUpInside];
    [goButton setTitle:@"GO" forState:UIControlStateNormal];
    [goButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [goButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goButton.titleLabel setFont:[UIFont fontWithName:@"Noteworthy-bold" size:20]];
    [goButton.layer setCornerRadius:8.0f];
    [goButton setAlpha:0.8];
    [goButton setClipsToBounds:YES];
    [self.containerView addSubview:goButton];
    
    
    
    
    [self.tableView reloadData];
    /*p-=0
    
    // Create our Installation query
    PFQuery *pushQuery = [PFInstallation query];
    [pushQuery whereKey:@"user" equalTo:[PFUser currentUser]]; // Set channe
    [pushQuery whereKey:@"objectId" equalTo: [self.devicesArray objectAtIndex:1][0]];
    
    // Send push notification to query
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:pushQuery];
    [push setMessage:@"Giants scored against the A's! It's now 2-2."];
    [push sendPushInBackground];*/
    
}

-(void) closeView {
    
    [self.blackoutView removeFromSuperview];
    
    [self.containerView removeFromSuperview];
}

#pragma mark UItableViewDelegate Methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if( cell == nil ) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    UILabel *deviceName = [[UILabel alloc] initWithFrame:CGRectMake(25, 3, self.tableView.frame.size.width - 25, 40)];
    [cell addSubview:deviceName];
    
    
    if([[self.devicesArray objectAtIndex:indexPath.row][1] isEqualToString:[PFInstallation currentInstallation][@"devicename"]]){
        
        [deviceName setTextColor:[UIColor lightGrayColor]];
        [deviceName setText:[NSString stringWithFormat:@"%@ (Remote)",[[self.devicesArray objectAtIndex:indexPath.row] objectAtIndex:1]]];
        [cell setUserInteractionEnabled:NO];
        
    }
    else{
        
        [deviceName setTextColor:[UIColor darkGrayColor]];
        [deviceName setFont:[UIFont boldSystemFontOfSize:18]];
        [deviceName setText:[[self.devicesArray objectAtIndex:indexPath.row] objectAtIndex:1]];
        [cell setUserInteractionEnabled:YES];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.devicesArray.count;
}

-(void) goToRemoteView {
    
    if(self.selectedDevicesArray.count > 0){
   
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     RemoteViewController *remoteView = [storyboard instantiateViewControllerWithIdentifier:@"remote"];
     [self presentViewController: remoteView animated:YES completion:nil];
     [remoteView setUpView: self.selectedDevicesArray];
     
     [self closeView];
        NSLog(@"Selected Devices Array: %@", self.selectedDevicesArray);
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Devices Selected" message:@"You must select at least one device in order to start the remote." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    NSUInteger index = [[tableView indexPathsForVisibleRows] indexOfObject:indexPath];
    
    if (index != NSNotFound) {
        UITableViewCell *cell = [[tableView visibleCells] objectAtIndex:index];
        if ([cell accessoryType] == UITableViewCellAccessoryNone) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [self.selectedDevicesArray addObject:[self.devicesArray objectAtIndex:indexPath.row]];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            NSMutableArray *array = [[NSMutableArray alloc] init];
            array = [self.devicesArray objectAtIndex:indexPath.row];
            [self.selectedDevicesArray removeObjectIdenticalTo:array];
        }
    }
}

- (IBAction)captureClicked:(id)sender {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CaptureViewController *remoteView = [storyboard instantiateViewControllerWithIdentifier:@"capture"];
    [self presentViewController: remoteView animated:YES completion:nil];
}



- (UIImage *)normalizedImage:(UIImage *) imageToNorm{
    if (imageToNorm.imageOrientation == UIImageOrientationUp) return imageToNorm;
    
    UIGraphicsBeginImageContextWithOptions(imageToNorm.size, NO, imageToNorm.scale);
    [imageToNorm drawInRect:(CGRect){0, 0, imageToNorm.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

- (IBAction)logOut:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
    
    [PFUser logOut];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark ODPullDownControl


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    [[PFUser currentUser] fetch];
    [self.devicesArray removeAllObjects];
    [self.tableView setUserInteractionEnabled:NO];
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
   
        
    self.devicesArray = [[PFUser currentUser][@"devices"] mutableCopy];
    
    NSLog(@"Devices Array: %@", self.devicesArray);
    
    [self.tableView reloadData];
    
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
        [self.tableView setUserInteractionEnabled:YES];
    });
}
@end
