//
//  ViewController.h
//  PairMe
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "StartingViewController.h"


@interface ViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (strong, nonatomic) PFLogInViewController *logInController;


@end

