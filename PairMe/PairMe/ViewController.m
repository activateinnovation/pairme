//
//  ViewController.m
//  PairMe
//
//  Created by Taylor Korensky on 3/30/15.
//  Copyright (c) 2015 Taylor Korensky. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize logInController;

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
    logInController.fields = (PFLogInFieldsUsernameAndPassword
                              | PFLogInFieldsLogInButton
                              | PFLogInFieldsSignUpButton
                              | PFLogInFieldsPasswordForgotten);
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"status"];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"loggedIn"]){
        
        
        [PFUser logInWithUsernameInBackground: [[NSUserDefaults standardUserDefaults] objectForKey:@"username"] password: [[NSUserDefaults standardUserDefaults] objectForKey:@"password"]];
        
        
        PFInstallation *installation = [PFInstallation currentInstallation];
        installation[@"user"] = [PFUser currentUser];
        installation[@"devicename"] = [[UIDevice currentDevice] name];
        [installation save];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            StartingViewController *startingView = [storyboard instantiateViewControllerWithIdentifier:@"startingView"];
            [self.logInController presentViewController: startingView animated:YES completion:nil];
        });
        
        
    }
    


    
}
- (void)viewDidLoad {
    [super viewDidLoad];
       // Do any additional setup after loading the view, typically from a nib.
}

-(void) viewDidAppear:(BOOL)animated{
    
   
    logInController.fields = (PFLogInFieldsUsernameAndPassword
                              | PFLogInFieldsLogInButton
                              | PFLogInFieldsSignUpButton
                              | PFLogInFieldsPasswordForgotten);
    logInController.view.backgroundColor = [UIColor lightGrayColor];
    UIImageView *logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PairMeLogoSmall.png"]];
    [logoView setContentMode:UIViewContentModeBottom];
    logInController.logInView.logo = logoView;

    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"background.png"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
   // [self.view addSubview:background];
    //[self.view sendSubviewToBack:background];
    
    [logInController.logInView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];

    [logInController.signUpController.signUpView addSubview:background];
    [logInController.signUpController.signUpView sendSubviewToBack:background];
    
   
    [logInController.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    [logInController.logInView.signUpButton.titleLabel setFont:[UIFont fontWithName:@"Noteworthy" size:20]];
    [logInController.logInView.passwordForgottenButton setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    logInController.logInView.usernameField.text = @"taylor";
    logInController.logInView.passwordField.text = @"rockon";
   
    UIImageView *logoView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PairMeLogoSmall.png"]];
    [logoView2 setContentMode:UIViewContentModeBottom];
     logInController.signUpController.delegate = self;
    logInController.signUpController.signUpView.logo = logoView2;
    [logInController.signUpController.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    [logInController.signUpController.signUpView.signUpButton.titleLabel setFont:[UIFont fontWithName:@"Noteworthy" size:20]];
    [logInController.signUpController.view setBackgroundColor:[UIColor lightGrayColor]];
    [logInController.signUpController.signUpView.dismissButton setBackgroundImage:[UIImage imageNamed:@"signUpButton.png"] forState:UIControlStateNormal];
    
    [self presentViewController:logInController animated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error{
    
    
    
}

-(void) logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user{
    
    NSLog(@"Success Login");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] setObject:user.username forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:user.password forKey:@"password"];
    
    NSLog(@"name: %@", [[UIDevice currentDevice] name]);
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = user;
    installation[@"devicename"] = [[UIDevice currentDevice] name];
    [installation save];
    
    NSLog(@"Installation: %@", installation);
    dispatch_async(dispatch_get_main_queue(), ^{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StartingViewController *startingView = [storyboard instantiateViewControllerWithIdentifier:@"startingView"];
    [self.logInController presentViewController: startingView animated:YES completion:nil];
    });
}

-(void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user{
    
    NSLog(@"name: %@", [[UIDevice currentDevice] name]);
    PFInstallation *installation = [PFInstallation currentInstallation];
    installation[@"user"] = user;
    installation[@"devicename"] = [[UIDevice currentDevice] name];
    [installation save];
    
    [PFUser logInWithUsernameInBackground: user.username password: user.password];
    NSLog(@"Success Signup");
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StartingViewController *startingView = [storyboard instantiateViewControllerWithIdentifier:@"startingView"];
    [self.logInController.signUpController presentViewController: startingView animated:YES completion:nil];
}


@end