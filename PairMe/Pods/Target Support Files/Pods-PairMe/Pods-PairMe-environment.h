
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 4

// ODRefreshControl
#define COCOAPODS_POD_AVAILABLE_ODRefreshControl
#define COCOAPODS_VERSION_MAJOR_ODRefreshControl 1
#define COCOAPODS_VERSION_MINOR_ODRefreshControl 1
#define COCOAPODS_VERSION_PATCH_ODRefreshControl 0

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
#define COCOAPODS_VERSION_MAJOR_Parse 1
#define COCOAPODS_VERSION_MINOR_Parse 7
#define COCOAPODS_VERSION_PATCH_Parse 1

// ParseUI
#define COCOAPODS_POD_AVAILABLE_ParseUI
#define COCOAPODS_VERSION_MAJOR_ParseUI 1
#define COCOAPODS_VERSION_MINOR_ParseUI 1
#define COCOAPODS_VERSION_PATCH_ParseUI 3

// UIImage-Resize
#define COCOAPODS_POD_AVAILABLE_UIImage_Resize
#define COCOAPODS_VERSION_MAJOR_UIImage_Resize 1
#define COCOAPODS_VERSION_MINOR_UIImage_Resize 0
#define COCOAPODS_VERSION_PATCH_UIImage_Resize 1

// UIImage-ResizeMagick
#define COCOAPODS_POD_AVAILABLE_UIImage_ResizeMagick
#define COCOAPODS_VERSION_MAJOR_UIImage_ResizeMagick 0
#define COCOAPODS_VERSION_MINOR_UIImage_ResizeMagick 0
#define COCOAPODS_VERSION_PATCH_UIImage_ResizeMagick 1

